const typingText = document.querySelector(".typing-text");
const inputText = document.querySelector("#input-text");
let yourScore = document.querySelector(".your-score");
let words = "";
const ismInput = document.querySelector("#ism");
function randWords() {
  let w1 = `The promise to Abraham came true because he trusted a promise and chose to live differently than others. Think how likely it was for this promise to have failed, but instead it has happened, and is continuing to unfold, as it was stated thousands of years ago.`;
  let w2 = `Salom mening ismim Muhammad, Front end dasturchiman, bu o'yin sizga yozish tezligingizni va xatosiz yozishingizda yordam beradi degan umiddaman`;
  let w3 = `There is something else promised as well. The blessing was not only for Abraham. It says that all peoples on earth will be blessed through you through Abraham. We should pay attention.`;
  let w4 = `This promise for a blessing includes everybody alive today! How? When? What kind of blessing? This is not clearly stated here but since we know that the first parts of this promise have come true`;
  let w5 = `No one else in all history is so well-known only because of descendants rather than from great achievements in his own life.`;
  let w6 = `Their unique greatness happened because God made it happen rather than some ability, conquest or power of their own.`;
  let w7 = `At that time the name of Abraham was not well-known so the promise came true only after it was written down, not before`;
  let w8 = `A look at Israels history in the book of Genesis in the Bible reveals that 4000 years ago a man, who is now very well known, went on a camping trip in that part of the world.  The Bible says that his story affects our future.`;
  let arr = [w1, w2, w3, w4, w5, w6, w7, w8];
  let ww = arr[parseInt(Math.random() * arr.length)];
  console.log(ww);
  //   words = ww;
  return ww;
}

document.querySelector(".ok-btn").addEventListener("click", function (e) {
  e.preventDefault();
  if (ismInput.value != 0) {
    document.querySelector(".name").textContent = ismInput.value;
    document.querySelector(".layer").classList.add("d-none");
    document.querySelector(".card").classList.add("d-none");
    randWords();
  }
});
words = randWords();
console.log("hi" + words);
let score = 0;
let textToArray = words.split(" ");

for (let i = 0; i < textToArray.length; i++) {
  let soz = document.createElement("span");
  soz.classList.add("soz");
  soz.textContent = textToArray[i] + " ";
  typingText.append(soz);
}
const sozSpan = document.querySelectorAll(".soz");
sozSpan[0].style.backgroundColor = "gray";
inputText.setAttribute(
  "placeholder",
  `${sozSpan[0].textContent} (enter text here)`
);
let j = 0;
sozSpan[j].style.fontSize = "42px";

function check(event) {
  console.log(j);
  console.log(textToArray.length);
  if (
    inputText.value.trim().length === textToArray[j].length &&
    inputText.value.trim() === textToArray[j] &&
    event.code == "Space"
  ) {
    sozSpan[j].style.backgroundColor = "rgb(2, 182, 2)";
    score++;
    nextWord();
  } else if (
    (inputText.value.length == textToArray[j].length &&
      inputText.value !== textToArray[j]) ||
    (inputText.value.length > textToArray[j].length - 1 &&
      event.code == "Space")
  ) {
    sozSpan[j].style.backgroundColor = "tomato";
    nextWord();
  }
}
function nextWord() {
  inputText.value = "";
  yourScore.textContent = score;
  if (j == textToArray.length - 1) {
    console.log(document.querySelector(".layer"));
    document.querySelector(".layer").classList.remove("d-none");
    document.querySelector(".card").classList.remove("d-none");
    document.querySelector(".card").classList.add("text-light");
    document.querySelector(
      ".card"
    ).innerHTML = `You typed ${score} words correctly`;
  }
  sozSpan[j + 1].style.backgroundColor = "gray";
  sozSpan[j + 1].style.fontSize = "42px";
  sozSpan[j].style.fontSize = "24px";
  inputText.setAttribute("placeholder", `${sozSpan[j + 1].textContent}`);

  j++;
}
